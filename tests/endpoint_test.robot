*** Variables ***
${URL}      ${Staging.URL}

*** Settings ***
### FIX: need the appropriate student's IP for staging environment
Library    String
Library    REST           ${URL}          ssl_verify=false

*** Test Cases ***
### FIX: expected response body
Get JSON
  GET       /json
  Output    response body
  Object    response body
  String    response body data  hello,world

### Add two more tests for the "other" interfaces and get them to pass

