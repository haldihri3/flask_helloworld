"""test cases for helloworld"""

from helloworld import app

def test_home():
    """test the root endpoint"""
    response = app.test_client().get('/', content_type='html/text')
    assert response.status_code == 200
    ## FIX 2: correct expected response
    assert b'DevSecOps Hello Flask App' in response.data

def test_other():
    """test the handling of non-existent endpoints"""
    response = app.test_client().get('a', content_type='html/text')
    assert response.status_code == 404
    assert response.data == b'The page named a does not exist.'
