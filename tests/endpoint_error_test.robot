*** Settings ***
Library    Selenium2Library
Suite Teardown  Close All Browsers

*** Variables ***
### FIX: need the appropriate student IP for staging environment.
### Go look in robot.yml.
${URL}      ${Staging.URL}/blah
${BROWSER}  Chrome

*** Test Cases ***
Example Error
  ${chrome_options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
  Call Method    ${chrome_options}   add_argument    headless
  Call Method    ${chrome_options}   add_argument    disable-gpu
  Call Method    ${chrome_options}   add_argument    no-sandbox

  ${options}=     Call Method     ${chrome_options}    to_capabilities
  Open Browser    ${URL}   browser=chrome  desired_capabilities=${options}
  Selenium2Library.Wait Until Element Is Visible  xpath=//body
  Element Should Contain  //body  blah
